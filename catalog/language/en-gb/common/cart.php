<?php
// Text
$_['text_items']     = '<span class="text-top-cart">Cart</span><span class="number-top-cart"> %s item(s)</span>';
$_['text_empty']     = 'Your shopping cart is empty!';
$_['text_cart']      = 'View Cart';
$_['text_checkout']  = 'Checkout';
$_['text_recurring'] = 'Payment Profile';