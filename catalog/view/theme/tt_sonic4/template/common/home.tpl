<?php echo $header; ?>
<div class="container">
<div class="row">
    <div class="col-md-4 hidden-sm hidden-xs"><div class="banner7_left"><?php echo $banner7_left; ?></div></div>
    <div class="col-md-8 col-sm-12"><div class="banner7-home"><?php echo $banner7; ?></div></div>
</div>
<div class="block-content-top">
    <div class="row"><?php echo $block_top; ?></div>
</div> 
</div>

<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9 col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<div class="end-content-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <?php echo $block_service; ?>
            </div>
            <div class="col-md-4 col-sm-12">
                <?php echo $tesmonial; ?>
            </div>
            <div class="col-md-4 col-sm-12">
                <?php echo $module_blog; ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>