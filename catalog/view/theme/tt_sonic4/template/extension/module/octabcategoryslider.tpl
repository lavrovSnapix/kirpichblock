<?php $tab_effect = 'wiggle'; ?>
<script type="text/javascript">
$(document).ready(function() {
	$(".<?php echo $cateogry_alias;?> .tab_content_category").hide();
	$(".<?php echo $cateogry_alias;?> .tab_content_category:first").show(); 
    $(".<?php echo $cateogry_alias;?> ul.tabs-categorys li:first").addClass("active");
	$(".<?php echo $cateogry_alias;?> ul.tabs-categorys li").click(function() {
		$(".<?php echo $cateogry_alias;?> ul.tabs-categorys li").removeClass("active");
		$(this).addClass("active");
		$(".<?php echo $cateogry_alias;?> .tab_content_category").hide();
		$(".<?php echo $cateogry_alias;?> .tab_content_category").removeClass("animate1 <?php echo $tab_effect;?>");
		var activeTab = $(this).attr("rel"); 
		$("#"+activeTab) .addClass("animate1 <?php echo $tab_effect;?>");
		$("#"+activeTab).fadeIn(); 
	});
});
</script>
<?php 
	$row=1;
	$row = $config_slide['f_rows']; 
	if(!$row) {$row=1;}
?>
<div class="product-tabs-category-container-slider special-button-owl <?php echo $cateogry_alias;?>">
    <div class="tab-title group-title">
        <h3 class="title"><?php echo $title ?></h3>
        <ul class="tabs-categorys"> 
    	<?php $count=0; ?>
    	<?php foreach($category_products as $cate_id => $products ){ ?>
    		<li rel="tab_cate<?php echo $cate_id; ?>"  >
    			<!-- <span class="title-category">
    				<?php if (isset ($array_cates[$cate_id]['name'])) echo $array_cates[$cate_id]['name']; ?>
    			</span> -->
				
    		</li>
    			<?php $count= $count+1; ?>
    	<?php } ?>	
    	</ul>
    	<div class="content-img img img-transiton1">
    	<div class="row">
		<?php if($fimage || $simage): ?>
			<?php if($fimage): ?>
				<div class="col-md-8">
				<a href="#">
					<img class="thumb-img img-first" src="<?php echo $fimage; ?>" alt="" />
				</a>
				</div>
			<?php endif; ?>
			<?php if($simage): ?>
				<div class="col-md-4">
				<a href="#">
					<img class="thumb-img img-second" src="<?php echo $simage; ?>" alt="" />
				</a>
				</div>
			<?php endif; ?>
		<?php endif; ?>
		</div>
		</div>
    </div><!-- tab-title -->
	
		<div class="tab_container_category"> 
		<?php foreach($category_products as $cate_id => $products ){ ?>
			<div id="tab_cate<?php echo $cate_id; ?>" class="tab_content_category">
				<ul class="productTabContent owl-demo-tabcate">
				<?php $i=0; ?>
				<?php foreach ($products as $product){ ?>
							<?php if($i++ % $row ==0){  echo  "<li class='row_item'><div class='product-thumb'>"; } ?>
					<div class="item">
						<div class="item-inner">
							<div class="oc-box-content">
								<div class="products">
									<div class="product-img-content">
										<a class="product-img" href="<?php echo $product['href']; ?>">
											<div class="product-image">
												<?php if($product['rotator_image']): ?>
													<img class="img2" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>" />
												<?php endif; ?>
												<img class="img1" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
											</div>
										</a>
										<?php if($config_slide['tab_cate_show_addtocart']) { ?>
										<div class="add-to-links">
											<button class="btn-wishlist" type="button"  title="<?php echo $button_wishlist; ?>"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
												<span class="effect-btn effect-wishlist">
													<i class="fa fa-heart-o"></i>
												</span>
												<em class="tooltips"><?php echo $tooltip_wishlist; ?></em>
											</button>
											<button class="btn-compare" type="button"  title="<?php echo $button_compare; ?>"  onclick="compare.add('<?php echo $product['product_id']; ?>');">
												<span class="effect-btn effect-compare">
													<i class="fa fa-refresh"></i>
												</span>
												<em class="tooltips"><?php echo $tooltip_compare; ?></em>
											</button>
										</div> <!-- end-add-to-link -->
										<?php } ?>

										<?php if ($product['is_new']):
											if($product['special']): ?>
												<div class="label-pro-sale"><?php echo $text_sale; ?></div>
											<?php else: ?>
												<div class="label-pro-new"><?php echo $text_new; ?></div>
											<?php endif; ?>
										<?php else: ?>
											<?php if($product['special']): ?>
												<div class="label-pro-sale"><?php echo $text_sale; ?></div>
											<?php endif; ?>
										<?php endif; ?>
										
									</div><!-- product-img-content -->
									<?php if($config_slide['tab_cate_show_des']) { ?>
										<div class="des"><?php echo $product['description']; ?></div>
									<?php } ?>
									<div class="top-inner">
										<h2 class="product-name">
											<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
										</h2>
										<?php if (isset($product['rating'])) { ?>
											<div class="rating"><img src="catalog/view/theme/tt_sonic1/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
										<?php } ?>
										<?php if($config_slide['tab_cate_show_price']) { ?>
											<?php if ($product['price']) { ?>
											<div class="price-box">
												<?php if (!$product['special']) { ?>
													<span class="price"><?php echo $product['price']; ?></span>
												<?php } else { ?>
													<p class="old-price"><span class="price"><?php echo $product['price']; ?></span></p>
													<p class="special-price"><span class="price"><?php echo $product['special']; ?></span></p>
												<?php } ?>
											</div>
											<?php } ?>
										<?php } ?>
										
									</div><!-- top-inner -->
									<div class="actions">
										<?php if($config_slide['tab_cate_show_addtocart']) { ?>
											<button class="button btn-cart" type="button"  title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');">
												<i class="fa fa-plus"></i>
												<span class="effect-add-cart" data-hover="<?php echo $button_cart; ?>">
													<?php echo $button_cart; ?>
												</span>
											</button>
										<?php } ?>
									</div><!-- actions -->
								</div><!-- products -->
							</div><!-- oc-box-content -->
						</div> <!-- item-inner -->
					</div> <!-- item -->
					<?php if($i % $row == 0 ): ?>
					</div></li><!-- row_items-->
				 <?php else: ?>
					<?php if($i == count($products)): ?>
						</div></li><!-- row_items-->
					<?php endif; ?>
				 <?php endif; ?>
				<?php } ?>
				</ul><!-- productTabContent -->
			</div>
		<?php } ?>
	 </div> <!-- .tab_container_category -->
</div><!-- <?php echo $cateogry_alias;?> -->
<script type="text/javascript">
$(document).ready(function() { 
  $(".<?php echo $cateogry_alias;?> .owl-demo-tabcate").owlCarousel({
	autoPlay: <?php if($config_slide['tab_cate_autoplay']) { echo 'true' ;} else { echo 'false';} ?>,
	items : <?php if($config_slide['items']) { echo $config_slide['items'] ;} else { echo 3;} ?>,
	slideSpeed : <?php if($config_slide['tab_cate_speed_slide']) { echo $config_slide['tab_cate_speed_slide'] ;} else { echo 200;} ?>,
	navigation : <?php if($config_slide['tab_cate_show_nextback']) { echo 'true' ;} else { echo 'false';} ?>,
	paginationNumbers : true,
	pagination : <?php if($config_slide['tab_cate_show_ctr']) { echo 'true' ;} else { echo 'false';} ?>,
	stopOnHover : false,
	itemsDesktop : [1199,4], 
	itemsDesktopSmall : [991,3], 
	itemsTablet: [767,2], 
	itemsMobile : [479,1]
  });
});
</script>