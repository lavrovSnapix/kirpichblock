<?php echo $header; ?>
<div class="container">
    <div class="row">
        <h1 style="display: none">Строительные материалы с доставкой
            по Москве и Московской области</h1>
        <div class="main-content">
            <div class="col-md-3 mobile-none">
                <div class="sidebar">
                    <?php echo $column_left; ?>
                    <div class="col-md-12 col-sm-3" style="margin-bottom:25px">  <?php echo $tesmonial; ?>  </div>
                    <div class="our-service"><h4 class="our-service-title"><span>Наш сервис</span></h4>
                        <ul>
                            <li><i><img src="image/icon1.png" alt="" class="home-icon"></i>
                                <h2>Строительная организация?</h2>
                                <p>Предусмотрены трехсторонние переговоры по снижению цен</p>
                                </li>
                            <li><i><img src="image/icon2.png" alt="" class="home-icon"></i>
                                <h2 style="margin-top: 10px">Дополнительная скидка до 10%</h2>
                                <p>Для заказов от 200 тысяч штук к указанной цене</p>
                                </li>
                            <li><i><img src="image/icon3.png" alt="" class="home-icon"></i>
                                <h2>Оплата</h2>
                                <p>Оплата по факту доставки</p>
                                </li>
                            <li><i><img src="image/icon4.png" alt="" class="home-icon"></i>
                                <h2>Бесплатные расчеты необходимого товара</h2>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="right-content">
                    <div class="banner7-home">
                        <?php echo $banner7; ?>
                    </div>
                    <div class="block-content-top">
                        <div id="cmsblock-19" class="cmsblock">
                            <div class="description">
                                <div class="static-block">
                                    <div class="row">
                                        <div class="img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12"><a title=""
                                                                                                                 href="https://kirpichblock.ru/index.php?route=product/category&path=65"
                                                                                                                 data-exist="1"
                                                                                                                 id="kirpich"> <img
                                                        src="image/loader.gif" alt=""
                                                data-src="image/catalog/demo/image-block/kirpich.jpg" class="lazyload"> </a>
                                            <div class="img-name" onclick="document.getElementById('kirpich').click()">Кирпич</div>
                                        </div>
                                        <div class="img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12"><a title=""
                                                                                                                 href="https://kirpichblock.ru/index.php?route=product/category&path=71"
                                                                                                                 data-exist="1"
                                                                                                                 id="keramika"> <img
                                                        src="image/loader.gif"
                                                        data-src="image/catalog/demo/image-block/keramika.jpg" alt="" class="lazyload"> </a>
                                            <div class="img-name" onclick="document.getElementById('keramika').click()">Теплая керамика</div>
                                        </div>
                                        <div class="img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12"><a title=""
                                                                                                                 href="https://kirpichblock.ru/index.php?route=product/category&path=99"
                                                                                                                 data-exist="1"
                                                                                                                 id="bloki"> <img
                                                        src="image/loader.gif"
                                                        data-src="image/catalog/demo/image-block/blocki.jpg" alt=""
                                                        style="height: 240px;object-fit: cover" class="lazyload"> </a>
                                            <div class="img-name" onclick="document.getElementById('bloki').click()">Газобетонные Блоки</div>
                                        </div>
                                        <div class="img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12"><a title=""
                                                                                                                 href="https://kirpichblock.ru/index.php?route=product/category&path=98"
                                                                                                                 data-exist="1"
                                                                                                                 id="bloki2"> <img
                                                        src="image/loader.gif"
                                                        data-src="image/catalog/demo/image-block/betonnye.jpg" alt=""
                                                        style="height: 240px;object-fit: cover" class="lazyload"> </a>
                                            <div class="img-name" onclick="document.getElementById('bloki2').click()">Бетонные Блоки</div>
                                        </div>
                                        <div class="img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12"><a title=""
                                                                                                                 href="https://kirpichblock.ru/index.php?route=product/category&path=108"
                                                                                                                 data-exist="1"
                                                                                                                 id="zbi"> <img
                                                        src="image/loader.gif"
                                                        data-src="image/catalog/demo/image-block/zhbi.jpg" alt="" class="lazyload"> </a>
                                            <div class="img-name" onclick="document.getElementById('zbi').click()">ЖБИ</div>
                                        </div>
                                        <div class="img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12"><a title=""
                                                                                                                 href="https://kirpichblock.ru/index.php?route=product/category&path=73"
                                                                                                                 data-exist="1"
                                                                                                                 id="smesi"> <img
                                                        src="image/loader.gif"
                                                        data-src="image/catalog/demo/image-block/smesi.jpg" alt="" class="lazyload"> </a>
                                            <div class="img-name" onclick="document.getElementById('smesi').click()">Строительные смеси и цемент</div>
                                        </div>
                                        <div class="img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12"><a title=""
                                                                                                                 href="https://kirpichblock.ru/index.php?route=product/category&path=137"
                                                                                                                 data-exist="1"
                                                                                                                 id="krovlya"> <img
                                                        src="image/loader.gif"
                                                        data-src="image/catalog/demo/image-block/p1.jpg" alt="" class="lazyload"> </a>
                                            <div class="img-name" onclick="document.getElementById('krovlya').click()">Кровельные материалы</div>
                                        </div>
                                        <div class="img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12"><a title=""
                                                                                                                 href="https://kirpichblock.ru/index.php?route=product/category&path=121"
                                                                                                                 data-exist="1"
                                                                                                                 id="plitka"> <img
                                                        src="image/loader.gif"
                                                        data-src="image/catalog/demo/image-block/plitka.jpg" alt=""
                                                class="lazyload"> </a>
                                            <div class="img-name" onclick="document.getElementById('plitka').click()">Тротуарная плитка</div>
                                        </div>
                                        <div class="img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12"><a title=""
                                                                                                                 href="https://kirpichblock.ru/index.php?route=product/category&path=162"
                                                                                                                 data-exist="1"
                                                                                                                 id="beton"> <img
                                                        src="image/loader.gif"
                                                        data-src="image/catalog/demo/image-block/beton.jpg" alt="" class="lazyload"> </a>
                                            <div class="img-name" onclick="document.getElementById('beton').click()">Бетон</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>       <!-- <?php echo $block_top; ?>  --></div>

                    <div class="row">    <?php if ($column_left && $column_right) { ?>    <?php $class = 'col-sm-6'; ?>    <?php } elseif ($column_left || $column_right) { ?>    <?php $class = 'col-sm-12'; ?>    <?php } else { ?>    <?php $class = 'col-sm-12'; ?>    <?php } ?>
                        <div id="content" class="action-block <?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
                        <?php echo $column_right; ?></div>
                    <div class="end-content-bottom">

                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div id="cmsblock-22" class="cmsblock">
                                    <div class="description">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">  <?php echo $module_blog; ?> </div>
                        </div>

                    </div>
                </div>
                <div class="partners">
                    <h3>Наши партнеры</h3>
                    <ul class="list">
                        <li><img src="image/loader.gif" data-src="image/Braer_logo.jpg" alt="" class="lazyload"></li>
                        <li><img src="image/loader.gif" data-src="image/zhkz-logo.jpg" alt="" class="lazyload"></li>
                        <li><img src="image/loader.gif" data-src="image/gkz.jpg" alt="" class="lazyload"></li>
                        <li><img src="image/loader.gif" data-src="image/wein.png" alt="" class="lazyload"></li>
                       <!-- <li><img src="image/aleksin.jpg" alt=""></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>