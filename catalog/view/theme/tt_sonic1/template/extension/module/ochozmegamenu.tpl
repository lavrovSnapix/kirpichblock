<div class="ma-nav-mobile-container visible-xs  hidden-md hidden-lg">
	<div class="hozmenu">
		<div class="navbar">
			<div id="navbar-inner" class="navbar-inner navbar-inactive">
          <div class="menu-mobile">
              <a class="btn btn-navbar navbar-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <span class="brand navbar-brand">Категории</span>
          </div>
					<?php echo $_menu ?>
			</div>
		</div>
	</div>
</div>
<div class="nav-container visible-lg visible-md visible-sm">
	<div class="nav1">
		<div class="nav2">
			<div id="pt_custommenu" class="pt_custommenu">

			<?php echo $hozmegamenu; ?>
			</div>
		</div>
		<div class="nav3 quick-phone hidden">
			<ul class="list-inline">
				<li><a href="tel:+78002222496"> <i
								class="fa fa-phone"></i><span
								class=""> 8-800-222-24-96 </span> </a></li>
			</ul>
		</div>
	</div>

</div>
<script>
//<![CDATA[
	var body_class = $('body').attr('class'); 
	if(body_class == 'common-home') {
		$('#pt_menu_home').addClass('act');
	}
	
var CUSTOMMENU_POPUP_EFFECT = <?php echo $effect; ?>;
var CUSTOMMENU_POPUP_TOP_OFFSET = <?php echo $top_offset ; ?>

//]]>
</script>
<script>
	$(function () {
      $(window).scroll(function () {
       if ($(this).scrollTop() > 180) {
        $('.nav-container').addClass("fix-nav");
       } else {
        $('.nav-container').removeClass("fix-nav");
       }
      });
     });
	$(function () {
      $(window).scroll(function () {
       if ($(this).scrollTop() > 180) {
        $('.quick-access').addClass("fix-header");
        $('.quick-phone').removeClass("hidden");
       } else {
        $('.quick-access').removeClass("fix-header");
        $('.quick-phone').addClass("hidden");
       }
      });
     });
</script>