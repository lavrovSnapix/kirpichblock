<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');

				$data['text_category_footer'] = $this->language->get('text_category_footer');
				$data['text_category_footer_laptop'] = $this->language->get('text_category_footer_laptop');
				$data['text_category_footer_mobile'] = $this->language->get('text_category_footer_mobile');
				$data['text_category_footer_tablet'] = $this->language->get('text_category_footer_tablet');
				$data['text_category_footer_headphone'] = $this->language->get('text_category_footer_headphone');
				$data['text_category_footer_camera'] = $this->language->get('text_category_footer_camera');
				$data['text_category_footer_accessories'] = $this->language->get('text_category_footer_accessories');
				$data['text_blog'] = $this->language->get('text_blog');
				$data['text_login'] = $this->language->get('text_login');
				$data['text_search'] = $this->language->get('text_search');
				$data['text_viewcart'] = $this->language->get('text_viewcart');
				$data['text_reward'] = $this->language->get('text_reward');
				$data['text_store_information'] = $this->language->get('text_store_information');
				$data['address'] = $this->config->get('config_address');
				$data['telephone'] = $this->config->get('config_telephone');
			    $data['email'] = $this->config->get('config_email');
			    $data['fax'] = $this->config->get('config_fax');
			    $data['title_address'] = $this->language->get('title_address');
			    $data['title_telephone'] = $this->language->get('title_telephone');
			    $data['title_fax'] = $this->language->get('title_fax');
			    $data['title_email'] = $this->language->get('title_email');
			

		$this->load->model('catalog/information');


				$data['blog'] = array(
					'title' => $this->config->get('ocblog_meta_title'),
					'href'  => $this->url->link('blog/blog')
				);
            
		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

		$data['contact'] = $this->url->link('information/information&information_id=10');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/account', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

				$data['footer1'] = $this->load->controller('common/footer1');
				$data['footer2'] = $this->load->controller('common/footer2');
				$data['newsletter'] = $this->load->controller('common/newsletter');
				$data['login'] = $this->url->link('account/login', '', 'SSL');
				$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
				$data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
				$data['blog'] = $this->url->link('blog/blog', '', 'SSL');
				$data['search'] = $this->url->link('product/search');
				$data['viewcart'] = $this->url->link('checkout/cart', '', 'SSL');
				$data['reward'] = $this->url->link('account/reward', '', 'SSL');
			

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		return $this->load->view('common/footer', $data);
	}
}
