<!DOCTYPE html><!--[if IE]><![endif]--><!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]--><!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]--><!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>"><!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1 ,maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>

<?php if ($noindex) { ?>
<!-- OCFilter Start -->
<meta name="robots" content="noindex,nofollow" />
<!-- OCFilter End -->
<?php } ?>
      
    <meta property="og:title" content="<?php echo $title; ?>" />
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>"/>
    <meta property="og:description" content=" <?php echo $description; ?>" />
    <?php } ?><?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <?php if ($ogimage) { ?>
    <meta property="og:image" content="<?php echo $ogimage; ?>" />
    <?php } ?>
    <?php if ($ogurl) { ?>
    <meta property="og:url" content="<?php echo $ogurl; ?>" />
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
    <script src="catalog/view/javascript/jquery/jquery-ui.js" defer></script>
    <script src="catalog/view/javascript/jquery.mask.js" defer></script>
    <script src="catalog/view/javascript/lazysizes.min.js" defer></script>
    <link href="catalog/view/javascript/jquery/css/jquery-ui.css" rel="stylesheet" media="screen">
    <link href="catalog/view/theme/tt_sonic1/stylesheet/opentheme/oclayerednavigation/css/oclayerednavigation.css"
          rel="stylesheet">
    <script src="catalog/view/javascript/opentheme/oclayerednavigation/oclayerednavigation.js" defer></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" defer></script>
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
    <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:300,600,900&display=swap&subset=cyrillic-ext" rel="stylesheet">-->
    <link href="catalog/view/theme/tt_sonic1/stylesheet/stylesheet.css" rel="stylesheet">
    <link href="catalog/view/theme/tt_sonic1/stylesheet/opentheme/ocsearchcategory/css/ocsearchcategory.css"
          rel="stylesheet">
    <script src="catalog/view/javascript/opentheme/ocajaxlogin/ocajaxlogin.js" defer></script>
    <link href="catalog/view/theme/tt_sonic1/stylesheet/opentheme/ocslideshow/ocslideshow.css" rel="stylesheet"/>
    <script src="catalog/view/javascript/opentheme/ocslideshow/jquery.nivo.slider.js" defer></script>
    <link href="catalog/view/theme/tt_sonic1/stylesheet/opentheme/hozmegamenu/css/custommenu.css" rel="stylesheet"/>
    <script src="catalog/view/javascript/opentheme/hozmegamenu/mobile_menu.js" defer></script>
    <script src="catalog/view/javascript/opentheme/hozmegamenu/custommenu.js" defer></script>
    <link href="catalog/view/theme/tt_sonic1/stylesheet/opentheme/vermegamenu/css/ocvermegamenu.css" rel="stylesheet"/>
    <script src="catalog/view/javascript/opentheme/vermegamenu/ver_menu.js" defer></script>
    <link rel="stylesheet" type="text/css"
          href="catalog/view/theme/tt_sonic1/stylesheet/opentheme/ocquickview/css/ocquickview.css">
    <script src="catalog/view/javascript/opentheme/ocquickview/ocquickview.js" defer></script>
    <script src="catalog/view/javascript/opentheme/owl-carousel/owl.carousel.min.js" defer></script>
    <link href="catalog/view/theme/tt_sonic1/stylesheet/opentheme/css/owl.carousel.css" rel="stylesheet"/>
    <script src="catalog/view/javascript/jquery/elevatezoom/jquery.elevatezoom.js" defer></script>
    <link href="catalog/view/theme/tt_sonic1//stylesheet/opentheme/css/animate.css" rel="stylesheet"/>
    <link rel="apple-touch-icon" sizes="60x60" href="image/apple-icon.png">
    <script async
            src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Abd39f4233321bbcb59df86f483c0554ca4a9b1a3a311392de99d4d2660e3d43a&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=false&amp;id=map-ryazan"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9w2x4aVfH1KlP6vumkMLt99c_JaRHvVA"></script>
    <script async
            src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A031a3e41d96775a25f3c3dbbef7d650103f2674ba2c9d5fd5db5aa8582a813f3&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=false&amp;id=map-kolomna"></script>
    <script async
            src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A2d9d37b849ad43d1b16ba0390d68d3254544f8d566da0f66b65938f094e98d18&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=false&amp;id=map-moscow"></script>
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <meta name="google-site-verification" content="KgOhjr5-_uOunKVXQZBpECk2-VSnRW9o61iMY7hpqqs" />
    <meta name="yandex-verification" content="8bcf97dc8508f43b" />
    <script src="catalog/view/javascript/common.js" defer></script>
    <script src="catalog/view/javascript/product.js" defer></script>
    <script src="//code-ya.jivosite.com/widget/UfjllfgGkO" async></script>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?><?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>"></script>
    <?php } ?><?php foreach ($analytics as $analytic) { ?><?php echo $analytic; ?><?php } ?></head>
<body class="<?php echo $class; ?>">
<nav id="top">
    <div class="container">
        <div class="pull-left left-top">
            <ul class="list-inline">
                <li class="top-clock"><a href="<?php echo $contact; ?>"> <i
                                class="fa fa-clock-o"></i>    Часы работы:   <span
                                class=""> 8:00 - 18:00 без выходных</span> </a></li>
                <li class="hidden-mb top-adress"><a href="<?php echo $contact; ?>"> <i
                                class="fa fa-map-marker"></i>            <?php echo $title_address; ?> : <span
                                class=""> <?php echo $address; ?></span> </a></li>
                <!-- <li>          <?php if ($logged) { ?>          <a id="a-logout-link" href="<?php echo $logout; ?>"><i class="fa fa-lock"></i><?php echo $text_logout; ?></a>          <?php } else { ?>          <a id="a-login-link" href="<?php echo $login; ?>"><i class="fa fa-unlock-alt"></i><?php echo $text_login; ?></a>          <?php } ?>        </li>-->
            </ul>
        </div>
        <div class="quick-access">
            <div class="top-search-content"><?php echo $search; ?></div>
            <div class="top-cart-content"><?php echo $cart; ?></div>
        </div>
        <!--<div id="top-links" class="nav pull-right right-top">      <ul class="list-inline">        <li><?php echo $currency; ?></li>        <li><?php echo $language; ?></li>        <li class="top-account">          <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle"><i class="fa fa-user"></i><span class="hidden-xs"><?php echo $text_account; ?></span><i class="fa fa-angle-down" aria-hidden="true"></i></a>          <ul class="dropdown-menu menu-top-account ul-account">            <?php if ($logged) { ?>            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>            <?php } else { ?>            <li><a id="a-register-link" href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>            <?php } ?>          </ul>        </li>      </ul>    </div>-->
    </div>
</nav>
<header>
    <div class="container">
        <div class="row vertical-header">
            <div class="col-md-6 col-sm-6">
                <div id="logo">          <?php if ($logo) { ?>          <a href="<?php echo $home; ?>"><img
                                src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"
                                class="img-responsive"/></a>          <?php } else { ?>          <h1><a
                                href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>          <?php } ?>
                    <p class="logo-text">Строительные материалы с доставкой <br/> по Москве и Московской области</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 d-flex">
                <div class="modals">
                    <button class="btn">Обратный звонок</button>
                </div>
                <div class="pull-right left-top">
                    <ul class="list-inline">
                        <li class="phone-header"><a href="tel:+78002222496">
                                <br/>
                                <i class="fa fa-phone"></i>  <span class="phone-ml"> 8 800 222-24-96 </span> </a></li>
                        <li class="mail-header"><a href="<?php echo $contact; ?>"> <i
                                        class="fa fa-envelope-o"></i>            <?php echo $title_email; ?> : <span
                                        class=""> <?php echo $email; ?></span> </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="top-menu">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 desktop-flex"><?php echo $hoz_menu; ?></div>
        </div>
    </div>
</div>

<div class="modal-cn hidden">
    <div class="callback-modal">
        <img src="image/catalog/-2.png" alt="логотип">
        <img  src=image/loader.gif" data-src="image/woman.png" alt="Менеджер" class="lazyload">
        <p class="headline">
            Оставьте заявку на обратный звонок и мы вам перезвоним
        </p>
        <form>
            <input type="tel" placeholder="Введите ваш телефон" name="phone" pattern="^[+]7[\s][(][0-9]{3}[)][\s][0-9]{3}[\-][0-9]{2}[\-][0-9]{2}$" required="" id="phone">
            <button type="submit" class="btn_submit" id="btn_submit">Узнать стоимость</button>
            <div class="form-group">
            </div>
        </form>                <p>Наши специалисты свяжутся с вами в самое ближайшее время!</p>
        <a href="https://kirpichblock.ru/privacy" class="privacy">Нажимая на кнопку «Отправить», я даю согласие на обработку персональных данных</a>
        <div class="close-modal">X</div>
    </div>
</div>

<div class="modal-product hidden">
    <div class="callback-modal">
        <img src="image/catalog/-2.png" alt="логотип">
        <img src="image/woman.png" alt="Менеджер" class="modal-img">
        <p class="headline modal-headline">
            Кирпич облицовочный TEREX Мокко шале
        </p>
        <form>
            <input type="tel" placeholder="Введите ваш телефон" name="phone" pattern="^[+]7[\s][(][0-9]{3}[)][\s][0-9]{3}[\-][0-9]{2}[\-][0-9]{2}$" required="" id="product-phone">
            <button type="submit" class="btn_submit" id="product-submit">Заказать</button>
            <div class="form-group">
            </div>
        </form>                <p>Наши специалисты свяжутся с вами в самое ближайшее время!</p>
        <a href="https://kirpichblock.ru/privacy" class="privacy">Нажимая на кнопку «Отправить», я даю согласие на обработку персональных данных</a>
        <div class="close-modal">X</div>
    </div>
</div>

<div id="top-button" style="opacity: 1;">
    <div class="icon">
        <svg version="1.1" width="24" height="16"><defs></defs><g><path stroke="none" d=" M 12.000099182128906 1.9989197254180908 C 12.000099182128906 1.9989197254180908 14.250236511230469 4.203514218330383 14.250236511230469 4.203514218330383 C 14.250236511230469 4.203514218330383 4.249244689941406 14.001556634902954 4.249244689941406 14.001556634902954 C 4.249244689941406 14.001556634902954 1.998992919921875 11.796962141990662 1.998992919921875 11.796962141990662 C 1.998992919921875 11.796962141990662 12.000099182128906 1.9989197254180908 12.000099182128906 1.9989197254180908 Z M 12.000099182128906 1.9989197254180908 C 12.000099182128906 1.9989197254180908 22.00109100341797 11.796962141990662 22.00109100341797 11.796962141990662 C 22.00109100341797 11.796962141990662 19.750953674316406 14.001556634902954 19.750953674316406 14.001556634902954 C 19.750953674316406 14.001556634902954 9.749847412109375 4.203514218330383 9.749847412109375 4.203514218330383 C 9.749847412109375 4.203514218330383 12.000099182128906 1.9989197254180908 12.000099182128906 1.9989197254180908 Z"></path></g></svg>
    </div>
</div>

<script>$(document).ready(function () {   //top-cart show subnav on hover    $('#cart').mouseenter(function() {      $(this).find(".dropdown-menu").stop(true, true).slideDown();    });    //hide submenus on exit    $('#cart').mouseleave(function() {      $(this).find(".dropdown-menu").stop(true, true).slideUp();    });  //top-currency show subnav on hover    $('#form-currency').mouseenter(function() {      $(this).find(".dropdown-menu").stop(true, true).slideDown();    });    //hide submenus on exit    $('#form-currency').mouseleave(function() {      $(this).find(".dropdown-menu").stop(true, true).slideUp();    });  //top-languge show subnav on hover    $('#form-language').mouseenter(function() {      $(this).find(".dropdown-menu").stop(true, true).slideDown();    });    //hide submenus on exit    $('#form-language').mouseleave(function() {      $(this).find(".dropdown-menu").stop(true, true).slideUp();    });  //top-account show subnav on hover    $('.top-account').mouseenter(function() {      $(this).find(".dropdown-menu").stop(true, true).slideDown();    });    //hide submenus on exit    $('.top-account').mouseleave(function() {      $(this).find(".dropdown-menu").stop(true, true).slideUp();    });     });</script>

<script>
    $(document).ready(function(){
        $('#btn_submit').click(function(){
            // собираем данные с формы
            var user_phone    = $('#phone').val();
            // отправляем данные
            $.ajax({
                url: "https://kirpichblock.ru/callback.php", // куда отправляем
                type: "post", // метод передачи
                dataType: "json", // тип передачи данных
                data: { // что отправляем
                    "user_phone":    user_phone
                },
                // после получения ответа сервера
                success: function(data){
                    $('.messages').html(data.result); // выводим ответ сервера
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function(){
        $('#product-submit').click(function(){
            // собираем данные с формы
            var user_phone    = $('#product-phone').val();
            var product_name    = $('.modal-headline').text();
            // отправляем данные
            $.ajax({
                url: "https://kirpichblock.ru/productCallback.php", // куда отправляем
                type: "post", // метод передачи
                dataType: "json", // тип передачи данных
                data: { // что отправляем
                    "user_phone":    user_phone,
                    "product_name":    product_name
                },
                // после получения ответа сервера
                success: function(data){
                    $('.messages').html(data.result); // выводим ответ сервера
                }
            });
        });
    });
</script>