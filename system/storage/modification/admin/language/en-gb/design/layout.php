<?php
// Heading
$_['heading_title']       = 'Layouts';

// Text
$_['text_success']        = 'Success: You have modified layouts!';
$_['text_list']           = 'Layout List';
$_['text_add']            = 'Add Layout';
$_['text_edit']           = 'Edit Layout';
$_['text_remove']         = 'Remove';
$_['text_route']          = 'Choose the store and routes to be used with this layout';
$_['text_module']         = 'Choose the position of the modules';
$_['text_default']        = 'Default';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';

				$_['text_hoz_menu']       = 'Block Hoz_menu';
				$_['text_ver_menu']       = 'Block Ver_menu';
				$_['text_banner7']        = 'Block Banner7';
				$_['text_banner7_left']   = 'Block Banner7 Left';
				$_['text_banner7_right']  = 'Block Banner7 Right';
				$_['text_block_top']      = 'Block Top';
				$_['text_footer1']        = 'Block Static Bottom';
				$_['text_footer2']        = 'Block Brands Logo';
				$_['text_newsletter']     = 'Block Newsletter';
				$_['text_block_service']  = 'Block Our Service';
				$_['text_tesmonial']      = 'Block Testimonial';
				$_['text_module_blog']    = 'Block Blog';
			
$_['text_column_right']   = 'Column Right';

// Column
$_['column_name']         = 'Layout Name';
$_['column_action']       = 'Action';

// Entry
$_['entry_name']          = 'Layout Name';
$_['entry_store']         = 'Store';
$_['entry_route']         = 'Route';
$_['entry_module']        = 'Module';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify layouts!';
$_['error_name']          = 'Layout Name must be between 3 and 64 characters!';
$_['error_default']       = 'Warning: This layout cannot be deleted as it is currently assigned as the default store layout!';
$_['error_store']         = 'Warning: This layout cannot be deleted as it is currently assigned to %s stores!';
$_['error_product']       = 'Warning: This layout cannot be deleted as it is currently assigned to %s products!';
$_['error_category']      = 'Warning: This layout cannot be deleted as it is currently assigned to %s categories!';
$_['error_information']   = 'Warning: This layout cannot be deleted as it is currently assigned to %s information pages!';