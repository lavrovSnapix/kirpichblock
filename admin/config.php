<?php
// HTTP
define('HTTP_SERVER', 'https://kirpichblock.ru/admin/');
define('HTTP_CATALOG', 'https://kirpichblock.ru/');

// HTTPS
define('HTTPS_SERVER', 'https://kirpichblock.ru/admin/');
define('HTTPS_CATALOG', 'https://kirpichblock.ru/');

// DIR
define('DIR_APPLICATION', '/home/o/oookbk/public_html/admin/');
define('DIR_SYSTEM', '/home/o/oookbk/public_html/system/');
define('DIR_IMAGE', '/home/o/oookbk/public_html/image/');
define('DIR_LANGUAGE', '/home/o/oookbk/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/o/oookbk/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/o/oookbk/public_html/system/config/');
define('DIR_CACHE', '/home/o/oookbk/public_html/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/o/oookbk/public_html/system/storage/download/');
define('DIR_LOGS', '/home/o/oookbk/public_html/system/storage/logs/');
define('DIR_MODIFICATION', '/home/o/oookbk/public_html/system/storage/modification/');
define('DIR_UPLOAD', '/home/o/oookbk/public_html/system/storage/upload/');
define('DIR_CATALOG', '/home/o/oookbk/public_html/catalog/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'oookbk');
define('DB_PASSWORD', 'k1BkS8hoip');
define('DB_DATABASE', 'oookbk');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
