<?php
// Heading
$_['heading_title']          = 'Minify';

// Text
$_['cache_del']              = 'Удаление кеша';
$_['minify_note']            = 'После изменения CSS и JS файлов нужно очищать кеш';
$_['minify_alert']           = 'Кеш успешно очищен';

// Error
$_['error_permission']        = 'У вас нет прав для изменения этого модуля!';